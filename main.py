import pandas as pd
import numpy as np

# NOTE: EXAMPLE
# (SUCCESSFULLY FORMATTED BY RUFF)

df = (
    pd.DataFrame(np.arange(300, 2000, 1)).astype(str).value_counts()
    .to_numpy()
    .reshape(-1, 1)
    .T
)
print("hello")

print(df)
